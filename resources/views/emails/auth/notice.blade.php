@component('mail::message')
    Добрый день,  {{ $manager->name }}!

    Только что в систему вошел пользователь:
    Имя: {{ $user->name }},
    Почта: {{ $user->email }}.

    Спасибо!
@endcomponent
