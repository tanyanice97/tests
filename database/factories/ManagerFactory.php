<?php

    /** @var \Illuminate\Database\Eloquent\Factory $factory */

    use App\Manager;
    use Faker\Generator as Faker;

    $factory->define(Manager::class, function (Faker $faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ];
    });
