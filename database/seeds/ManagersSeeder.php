<?php

    use App\Manager;
    use Illuminate\Database\Seeder;

    class ManagersSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            factory(Manager::class, 3)->create();
        }
    }
