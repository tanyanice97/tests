<?php

namespace App\Http\Controllers;

use App\Events\ClientVerified;
use App\Jobs\ManagerNotificationJob;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        ManagerNotificationJob::dispatch(auth()->user());
        return view('home');
    }
}
