<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifiedNotification extends Mailable
{
    use Queueable, SerializesModels;

    private $manager;

    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($manager, $user)
    {
        $this->manager = $manager;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.auth.notice', ['manager' => $this->manager, 'user' => $this->user])
            ->subject(config('app.name') . ":Notification")
            ->from(config('mail.from.address'));
    }
}
