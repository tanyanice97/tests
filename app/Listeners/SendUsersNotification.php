<?php

namespace App\Listeners;

use App\Events\ClientVerified;
use App\Mail\VerifiedNotification;
use App\Manager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendUsersNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ClientVerified  $event
     * @return void
     */
    public function handle(ClientVerified $event)
    {
        Mail::to($event->manager->email)->send(new VerifiedNotification($event->manager, $event->user));
    }
}
